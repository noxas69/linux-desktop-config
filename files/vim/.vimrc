let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


set encoding=utf-8
set nocompatible
syntax on
syntax enable
"set number
set ruler
set autoread
set tabstop=2
set shiftwidth=2
set expandtab
set smarttab
set autoindent
set hlsearch
filetype plugin on
filetype indent on
set laststatus=2
syn on
let g:dracula_colorterm = 0

function! CurDir()
    let curdir = substitute(getcwd(), "~/", "g")
    return curdir
endfunction

function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    else
        return ''
    endif
endfunction

" disable Ex mode
nmap Q <Nop>

"set noai
"set et

" When editing a file, always jump to the last known cursor position.
if has("autocmd")
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
\| exe "normal g'\"" | endif
endif


" correction
autocmd FileType tex setlocal spell spelllang=fr

" toogle between paste / nopaste
set pastetoggle=<F2>

" move tab easily
"nnoremap <C-Left> :tabprevious<CR>
"nnoremap <C-Right> :tabnext<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" vim-plug
call plug#begin('~/.vim/plugged')

Plug 'pearofducks/ansible-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bronson/vim-trailing-whitespace'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'dracula/vim', { 'as': 'dracula' }

call plug#end()
" end vim-plug
"autocmd BufRead,BufNewFile ~/depot/* set syntax=ansible
let g:ansible_attribute_highlight = "ob"

let vim_markdown_preview_pandoc=1
let vim_markdown_preview_browser='firefox'
let vim_markdown_preview_use_xdg_open=1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Appearance
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable
"set background=dark
colorscheme dracula

" cursor highlight
set cursorline
hi CursorLine term=bold cterm=bold guibg=Grey40
set cursorcolumn
"hi Cursorcolumn guibg=Grey40
" vim airline
let g:airline_powerline_fonts = 1
set t_Co=256
"let g:airline_theme='murmur'
let g:airline_theme='deus'
let g:airline#extensions#tabline#enabled = 1
map <C-L> <C-W>l
map <C-H> <C-W>h
let g:coc_disable_startup_warning = 1
nnoremap <C-p> :Files<Cr>
