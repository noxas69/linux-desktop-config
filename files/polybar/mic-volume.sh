#!/bin/sh

sources=$(pacmd list-sources | awk -F": " '/name:.*input/{print $2}' )
gm=0

for s in $sources
do
  v=$(pacmd list-sources | grep -A6 "name:.*$s"|grep "volume:" | awk -F/ '{gsub("%","");print $2}')
  m=$(pacmd list-sources | grep -A10 "name:.*$s"|awk -F": " '/muted:/ {print $2}')
  printf " "
  if `test "$m" = "yes"`
  then 
    printf "%%{F#337aff}MUTE"
    #printf "%%{B#f00}%%{F#fff}M"
  else
    gm=`expr $gm + 1`
    if `test $v -lt 20`
    then 
      printf "%%{F#337aff}_"
    elif `test $v -lt 40`
    then
      printf "%%{F#337aff}▁"
    elif `test $v -lt 60`
    then
      printf "%%{F#337aff}▂"
    elif `test $v -lt 80`
    then
      printf "%%{F#337aff}▄"
    else
      printf "%%{F#337aff}█"
    fi
  fi
done
if `test $gm -gt 0`
then
    printf "%%{B#f00}%%{F#fff} MICRO! "
else
    printf "%%{F#337aff}"
fi
